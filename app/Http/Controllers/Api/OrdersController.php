<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Orders;
use App\Models\Products;
use App\Models\Shops;
use App\Models\User;
use Illuminate\Http\Request;
use MongoDB\BSON\UTCDateTime;
use Storage;
use App\Events\OrderReceived;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $sid = Shops::where('user_id', auth()->user()->id)->first()->_id;
            $orders = Orders::where('shop_id',  $sid)->orderBy('created_at', 'DESC')->get();

            $data = [
                'data' => [
                    'orders' => $orders
                ],
            ];

            return $data;
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'msg' => 'Item doesn\'t exists', 'error' => $e];
        }
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function myOrders()
    {
        try {
            $orders = Orders::where('user_id', auth()->user()->id)->orderBy('created_at', 'DESC')->get();

            $data = [
                'data' => [
                    'orders' => $orders
                ],
            ];

            return $data;
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'msg' => 'Item doesn\'t exists', 'error' => $e];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($sid)
    {
        try {
            $orders = Orders::where('shop_id', $sid)->get();

            $data = [
                'data' => [
                    'orders' => $orders
                ],
            ];

            return $data;
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'msg' => 'Item doesn\'t exists', 'error' => $e];
        }      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
            try {
                $orderDetails = $request['order']; 
                $address = $orderDetails['address'];
                $phone = $orderDetails['phone'];
                $comment = $orderDetails['comment'];
                $userId = $orderDetails['user_id'];
                $order = $orderDetails['order'];
                $products = [];

                $i = 0;
                foreach($order as $item) {
                    if(isset($products[$item['_id']])) {
                        $products[$item['_id']]['quantity'] = $products[$item['_id']]['quantity'] + 1;
                    } else {
                        $products[$item['_id']] = $item;
                    }
                    $i = $i + 1;
                } 

                foreach($products as $id => $product) {
                    Products::where("_id", $id)->decrement('units', $product['quantity']);
                    Products::where("_id", $id)->increment('orders', $product['quantity']);
                           

                    $id = Orders::insertGetId([
                        'user_id' => $userId,
                        'product_id' => $id,
                        'shop_id' => $product['vendor']['_id'],   
                        'phone' => $phone,
                        'address' => $address['city'] . ', ' . $address['street'] . ' ' . $address['number'] . ', ' . $address['postal_code'],
                        'comment' => $comment,
                        'created_at' => new \MongoDB\BSON\UTCDateTime(new \DateTime('now')),
                        'status' => 1
                    ]);

                    event(new OrderReceived($orderDetails));
                }
                
                
                    return [
                         'statusCode' => 200,
                         'message' => 'Uspešno poručeno!',
                         'status' => 'success',
                    ];
            } catch (\Exception $e) {
                return $e;
                return ['statusCode' => 403, 'error' => $e];
            }
        }
}
