<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ApiController;
use App\Services\ProductsService;
use App\Models\Products;
use Illuminate\Http\Request;
use MongoDB\BSON\UTCDateTime;
use App\Http\Resources\ProductsCollection as ProductsCollection;
use Auth;

class ProductsController extends ApiController
{
    protected $limit = 100;

    private $productsService;

    /**
     * Bind Controller Service
     */
    public function __construct(ProductsService $productsService)
    {
        $this->productsService = $productsService;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = isset($request['page']) ? $request['page'] : 0;
        $limit = isset($request['limit']) ? (int) $request['limit'] : $this->limit;
        $skip = $limit * $page;
        
        try {
            $data = $this->productsService->paginateItems($skip, $limit);

            return ['statusCode' => 200, 'data' => $data];
        } catch (\Exception $e) {
            dd($e);
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request, $category, $q)
    {
        $page = isset($request['page']) ? $request['page'] : 0;
        $limit = isset($request['limit']) ? (int) $request['limit'] : $this->limit;
        $skip = $limit * $page;
        
        try {
            $data = $this->productsService->search($q, $category, $skip, $limit);

            if($data && is_array($data) && count($data) > 0) {
                return ['statusCode' => 200, 'data' => $data];
            }
            return ['statusCode' => 201, 'message' => 'No results.'];
        } catch (\Exception $e) {
            dd($e);
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }

        /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hash = md5(request()->ip());
        try {
            $product = Products::find($id);
            $views = !is_null($product->views) ? $product->views : [];

            if(!in_array($hash, array_column($views, 'hash'))) {
                $product->push('views', [ 
                    'hash' => $hash,
                    'created_at' => now(),
                    'ip' => request()->ip()
                ]);
            }
        
            $preview = $this->preview($product->first()->shop_id, $product->first()->_id);

            return [
                'statusCode' => 200,
                'data' => [
                    'product' => $product,
                    'shop' => $product->shop,
                    'preview' => $preview,
                    'itemCount' => count($preview),
                ],
            ];
        } catch (\Exception $e) {
            dd($e);
            return ['statusCode' => 403, 'msg' => 'Item doesn\'t exists', 'error' => $e];
        }
    }

    public function preview($sid, $pid)
    {
        try {
            $data = Products::where([
                ['units', '>', 0],
                ['deleted_at', '=', null],
                ['shop_id', '=', $sid],
                ['_id', '!=', $pid],
            ])->take(15)->orderBy('created_at', 'desc')->get()->keyBy('_id')->toArray();

            return $data;
        } catch (\Exception $e) {
            dd($e);
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $rate = $request->get("rate");
            return $request['rate'];
            
            if(isset($rate) && ($rate > 0 || $rate < 6)) {
                $shop = Products::where('_id', $id)->push('rating', [
                    'ip' => request()->ip(),
                    'rating' => $rate
                ]);
    
                return ['statusCode' => 200, 'status' => '1'];
            }

            return ['statusCode' => 200, 'status' => 0];
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }
}
