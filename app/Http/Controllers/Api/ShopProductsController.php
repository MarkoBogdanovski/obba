<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ApiController;
use App\Models\Products;
use App\Models\Shops;
use Illuminate\Http\Request;
use Storage;

class ShopProductsController extends ApiController
{
    protected $limit = 20;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $slug)
    {
        try {
            $exists = Shops::where('name', $slug)->exists();
            
            if ($exists) {
                $data = Shops::with(['products' => function($query) { 
                    $query->where([
                        ['units', '>', 0],
                        ['deleted_at', '=', null]
                    ])->orderBy('created_at', 'desc');
                }])->where('name', $slug)->first();
                
                if(isset($data->rating) && count($data->rating) > 0) {
                    $rating = 0;
                    foreach($data['rating'] as $rate) {
                        $rating += $rate['rating'];
                    }
                    $data['votes'] = count($data['rating']);
                    $data['rate'] = $rating / count($data['rating']);
                }
                
                unset($data['rating']);

                return [
                    'statusCode' => 200,
                    'data' => [
                        'shop' => $data,
                        'productCount' => count($data['products']),
                    ],
                ];
            }

            return ['statusCode' => 200, 'status' => 0];
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($shopId, $productId)
    {
        try {
            $data = Products::where([
                ['units', '>', 0],
                ['deleted_at', '=', null],
                ['shop_id', '=', $shopId],
                ['_id', '!=', $productId],
            ])->take($this->limit)->orderBy('created_at', 'desc')->get()->keyBy('_id')->toArray();

            return $data;
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }

}
