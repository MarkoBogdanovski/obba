<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ApiController;
use App\Models\Category;
use App\Models\Products;
use Illuminate\Http\Request;
use Storage;

class CategoryProductsController extends ApiController
{
    protected $limit = 100;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, string $category)
    {
        // Sample Result JSON
        if (!$this->mdbAvailable) {
            return json_decode(Storage::disk('local')->get('jsons/category/products.json'), JSON_PRETTY_PRINT);
        }

        $price = isset($request['price']) ? $request['price'] : false;
        $options = isset($request['options']) ? $request['options'] : false;
        $page = isset($request['page']) ? $request['page'] : 0;
        $limit = isset($request['limit']) ? $request['limit'] : $this->limit;
        $skip = $limit * $page;

        try {
            $category = Category::where('slug', $category)->first();

            if ($category !== null && $category->exists) {
                $products = Products::with(['shop'])->where([
                    ['units', '>', 0],
                    ['deleted_at', '=', null],
                    ['category_id', '=', $category->id],
                ])->skip($skip)->take($limit)->orderBy('created_at', 'desc')->get()->toArray();
                
                if(count($products) < 1) {
                    return [
                        "status" => 0,
                    ];
                }

                $brands = array_count_values(array_column($products, 'brand'));
                $prices = [min(array_column($products, 'price')), max(array_column($products, 'price'))];
                $options = array_values(array_unique(array_column($products, 'options'), SORT_REGULAR));

                return [
                    'data' => [
                        'products' => $products,
                        'priceFilter' => $prices,
                        'options' => $options,
                        'brands' => $brands,
                    ],
                ];
            }
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function preview($shopId, $productId)
    {
        try {
            $data = Products::where([
                ['units', '>', 0],
                ['deleted_at', '=', null],
                ['shop_id', '=', $shopId],
                ['_id', '!=', $productId],
            ])->take($this->limit)->orderBy('created_at', 'desc')->get()->keyBy('_id')->toArray();

            return $data;
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }

}
