<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ApiController;
use App\Models\Shops;
use Illuminate\Http\Request;
use MongoDB\BSON\UTCDateTime;
use Storage;

class ShopsController extends ApiController
{
    protected $limit = 15;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $slug)
    {
        // Sample Result JSON
        if (!$this->mdbAvailable) {
            return json_decode(Storage::disk('local')->get('jsons/shops/shop.json'), JSON_PRETTY_PRINT);
        }

        try {
            $data = Shops::with(['products' => function($query) { 
                    $query->where([
                        ['units', '>', 0],
                        ['deleted_at', '=', null]
                    ])->orderBy('created_at', 'desc');
            }])->where('name', $slug)->first()->toArray();

            $rating = 0;
            foreach($data['rating'] as $key => $value) {
                $rating =+ $value['rating'];
            }

            $data['rate'] = $rating / count($data['rating']);

            if ($data && is_array($data)) {
                return [
                    'data' => [
                        'shop' => $data,
                        'productCount' => count($data['products']),
                    ],
                ];
            }

            return [
                'status' => 0,
            ];
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($slug)
    {
        try {
            $shop = Shops::where('name', $slug)->first();

            if ($shop !== null && $shop->exists) {
                
                return [
                    "status" => 1,
                ];
            }

            return ["status" => 0];

        } catch (\Exception $e) {
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        try {
            $rate = $request->get("rate");
            
            if(isset($rate) && ($rate > 0 || $rate < 6)) {
                $shop = Shops::where('_id', $slug)->push('rating', [
                    'created_at' => now(),
                    'ip' => request()->ip(),
                    'rating' => $rate
                ]);
    
                return ['statusCode' => 200, 'status' => '1'];
            }

           // return ['statusCode' => 200, 'status' => 0];
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }
}
