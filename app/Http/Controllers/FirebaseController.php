<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;

class FirebaseController extends Controller
{
    public function index()
    {
		$serviceAccount = ServiceAccount::fromJsonFile(storage_path('/obba-acf73-firebase-adminsdk-lei7u-64a9bfb3ba.json'));
		$firebase = (new Factory)
                        ->withServiceAccount($serviceAccount)
                        ->withDatabaseUri('https://obba-acf73.firebaseio.com')
                        ->create();

		$database = $firebase->getDatabase();

		$newPost = $database
		                    ->getReference('address')
		                    ->push(['title' => 'Post title','body' => 'This should probably be longer.']);

		echo"<pre>";
		print_r($newPost->getvalue());
        echo "</pre>";
	}
}
