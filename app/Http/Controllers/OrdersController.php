<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Products;
use App\Models\Orders;
use App\Models\Shops;
use App\Models\User;
use \Carbon\Carbon;
use Illuminate\Http\Request;
use App\Notifications\OrderNotification;
use MongoDB\BSON\UTCDateTime;
use Auth;

class OrdersController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $orders = $request['order'];

        try {
            foreach($orders as $shopId => $items) {
                $products = [];

                if($shopId == 'address' || $shopId == 'comment' || $shopId == 'phone') {

                } else {
                    foreach($items as $item) {
                        if(isset($products[$item['_id']])) {
                            $products[$item['_id']]['quantity'] = $products[$item['_id']]['quantity'] + 1;
                        } else {
                            unset($item['shop']);
                            $products[$item['_id']] = $item;
                            $products[$item['_id']]['status'] = 0;
                            $products[$item['_id']]['quantity'] = 1;
                        }
                    } 

                    foreach($products as $id => $product) {
                        Products::where("_id", $id)->decrement('units', $product['quantity']);
                        Products::where("_id", $id)->increment('orders', $product['quantity']);
                    }

                    $id = Orders::insertGetId([
                        'user_id' => auth()->user()->id,
                        'order' => $products,
                        'shop_id' => $shopId,   
                        'phone' => $orders['phone'],
                        'address' => $orders['address']['city'] . ', ' . $orders['address']['street'] . ' ' . $orders['address']['number'] . ', ' . $orders['address']['postal_code'],
                        'comment' => $orders['comment'],
                        'created_at' => new \MongoDB\BSON\UTCDateTime(new \DateTime('now')),
                        'status' => 1
                    ]);
                        
                    $this->sendNotification($shopId, $id);
                }
            }

            return [
                'statusCode' => 200,
                'message' => 'Uspešno poručeno!',
                'status' => 'success',
            ];
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }

    public function sendNotification($shopId, $id) {
        try {
            $userId = Shops::where('_id', $shopId)->get()->pluck("user_id");
            $user = User::find($userId[0]);
            $user->notify(new OrderNotification($id));
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Orders $order)
    {
        try {
            $status = $request->get("status");

            $order->status = $status;
            $order->save();

            return [
                'data' => [
                    'order' => $order
                ]
            ];
        } catch (\Exception $e) {
            dd($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
