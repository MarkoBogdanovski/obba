<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\AddressService;
use App\Models\Address;
use Illuminate\Http\Request;
use MongoDB\BSON\UTCDateTime;
use Auth;


class AddressController extends Controller
{
    private $addressService;

    /**
     * Bind Controller Service
     */
    public function __construct(AddressService $addressService)
    {
        $this->addressService = $addressService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         try {
            $data = $this->addressService->all();
            return ['statusCode' => 200, 'data' => $data];
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        try {
            $this->addressService->insertGetId($request->all());
            return ['statusCode' => 200];
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($address)
    {
        try {
            $data = $this->addressService->getById($address);
            return [
                "statusCode" => 200,
                "data" => $data,
            ];
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $address)
    {
        try {
            $reset = Address::where([
                ['user_id', auth()->user()->id],
            ])->update(['default' => false, 'updated_at' => new UTCDateTime(now())]);

            $update = Address::where([
                ['user_id', auth()->user()->id],
                ['_id', "=", $address],
            ])->update(['default' => true, 'updated_at' => new UTCDateTime(now())]);

            return [ 'data' => 
                    [ 'status' => 'success' ]
                ];
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($address)
    {
        try {
            $this->addressService->deleteById($address);
            return ['statusCode' => 200];
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }
}
