<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Orders;
use Illuminate\Http\Request;
use Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $user->address;

        return ['statusCode' => 200, 'data' => $user];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {
        try {
            $available = User::where([
                ['username', $username]
            ])->doesntExist();

            if ($available) {
                return ['statusCode' => 200, 'status' => 0];
            }
            return ['statusCode' => 200, 'status' => 1];
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $data = [];
            $checkIfExists = true;

            $user = auth()->user();
            $username = isset($request['username']) ? $request['username'] : null;

            $hash = md5($request->get('email'));
            $shopDir = "public/users/avatars/" . $hash . "-" . $user->id;

            Storage::makeDirectory($shopDir);

            if (!empty($request->file('file'))) {
                $path = $request->file('file')->store($shopDir);
                $data['avatar'] = str_replace("/storage", "", Storage::url($path));
            }


            if (!empty($username) && $username != $user->username) {
                $checkIfExists = User::where('username', $username)->doesntExist();
                $data['username'] = $username;
            }

            $data['email'] = $request->get("email");
            $data['name'] = $request->get("name");
            $data['phone'] = $request->get("phone");

            User::where('id', $user->id)->update($data);

            return ['statusCode' => 200, 'success' => 1,];
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }
}
