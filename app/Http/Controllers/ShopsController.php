<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Shops;
use App\Models\User;
use App\Models\Products;
use Illuminate\Http\Request;
use MongoDB\BSON\UTCDateTime;
use Storage;

class ShopsController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $data = Shops::with(['orders.product', 'products' => function($query) { 
                $query->where([
                    ['units', '>', 0],
                    ['deleted_at', '=', null]
                ])->orderBy('created_at', 'desc');
            }])->where('user_id', auth()->user()->id)->first()->toArray();


            //$data["newOrders"] = auth()->user()->unreadNotifications;

            if ($data && is_array($data)) {
                return [
                    'data' => [
                        'shop' => $data,
                        'productCount' => count($data['products']),
                    ],
                ];
            }

            return [
                'status' => 0,
            ];
        } catch (\Exception $e) {
            dd($e);
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slug = trim(strtolower(str_replace(' ', '-', $request->get('name'))));
        $doesntExist = Shops::where('name', $slug)->doesntExist();
        
        if ($doesntExist) {
            try {
                $shopDir = "public/content/" . $slug . "/settings";

                Storage::makeDirectory($shopDir);

                $logo = $request->file('logo')->store($shopDir);

                $id = Shops::insertGetId([
                    'user_id' => auth()->user()->id,
                    'logo' => Storage::url($logo),
                    'name' => $slug,
                    'phone_number' => null,
                    'verified' => false,
                    'rating' => [],
                    'description' => $request->get('description'),
                    'display_name' => $request->get('display_name'),
                    'created_at' => new \MongoDB\BSON\UTCDateTime(new \DateTime('now')),
                    'status' => 'OPEN',
                ]);

                User::where('id', auth()->user()->id)->update(['owner' => 1]);

                return ['data' => [
                    'message' => 'Uspešno ste otvorili vašu prodavnicu!',
                    'status' => 'success',
                ]];
            } catch (\Exception $e) {
                return ['statusCode' => 403, 'error' => $e->getMessage()];
            }
        } else {
            return ['data' => [
                'message' => 'Korisničko ime je zauzeto!',
                'status' => 'warning',
            ]];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Shops $shop)
    {
        try {
            $shop->update($request->all());

            return response()->json($shop, 200);
        } catch (\Exception $e) {
            // $e
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Shops $shop)
    {
        try {
            $shop->delete();

            return response()->json(null, 204);
        } catch (\Exception $e) {
            // $e
        }
    }
}
