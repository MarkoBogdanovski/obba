<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Mail\WelcomeMail;
use Mail;

class AuthController extends Controller
{
    /**
     *     Register user
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'username' => 'string|min:5|max:144|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response(['data' => $validator->errors()], \Illuminate\Http\Response::HTTP_UNAUTHORIZED);
        }

        try {
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            $input['username'] = $input['username'];
            $user = User::create($input);

            $token = $user->createToken('SPA')->accessToken;

            Mail::to($input['email'])->send(new WelcomeMail($input['name']));

            return [
                'data' => [
                    'user' => $user,
                    'token' => $token,
                    'message' => 'Success',
                ],
            ];
        } catch (\Exception $e) {
            return [
                'data' => [
                    'status' => 'failed',
                    'message' => $e->getMessage()
                ]
            ];
        }
    }

    /**
     *   Login user
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (!auth()->attempt($credentials)) {
            return response()->json(['Authentication failed, try again.'], \Illuminate\Http\Response::HTTP_UNAUTHORIZED);
        }

        $user = auth()->user();
        $user->tokens()->delete();
        $token = $user->createToken('SPA')->accessToken;

        return [
            'data' => [
                'user' => $user,
                'token' => $token,
            ],
        ];
    }

    /**
     *  Logout user
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();

        $response = 'You have been succesfully logged out!';

        return [
            'response' => $response,
        ];
    }
}
