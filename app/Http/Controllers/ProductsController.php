<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\ProductsService;
use App\Models\Products;
use App\Models\Shops;
use Illuminate\Http\Request;
use EddTurtle\DirectUpload\Signature;
use Illuminate\Support\Str;
use Carbon\Carbon;
use MongoDB\BSON\UTCDateTime;

class ProductsController extends Controller
{
    protected $limit = 10;

    private $productsService;

    /**
     * Bind Controller Service
     */
    public function __construct(ProductsService $productsService)
    {
        $this->productsService = $productsService;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
    }

    /**
     * Upload product gallery images to AWS Bucket
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        try {
            $opts = ['acl' => 'public-read-write'];

            $upload = new Signature(
                "AKIAQGDPJ3AMGWD5TJFF",
                "/gU/9DE2R89GN0myieSlKld0aOHGSFlrL7Q9gRq3",
                "obba-products",
                "us-east-1",
                $opts
            );

            return [
                'signature' => $upload->getFormInputs(),
                'postEndpoint' => $upload->getFormUrl()
            ];
        } catch (\Exception $e) {
            return [
                'statusCode' => 403,
                'error' => $e->getMessage(),
            ];
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $this->productsService->insertGetId($request->all());
            return ['statusCode' => 200, 'data' => $data];
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Products $product)
    {
        try {
            $product->update($request->all());

            return response()->json($shop, 200);
        } catch (\Exception $e) {
            // $e
        }
    }

/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($product)
    {
        try {
            $this->productsService->deleteById($product);
            return ['statusCode' => 200];
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function preview($shopId, $productId)
    {
        try {
            $data = Products::where([
                ['units', '>', 0],
                ['deleted_at', '=', null],
                ['shop_id', '=', $shopId],
                ['_id', '!=', $productId],
            ])->take($this->limit)->orderBy('created_at', 'desc')->get()->keyBy('_id')->toArray();

            return $data;
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function shopProducts(Request $request, $slug)
    {
        $shop = Shops::where('name', $slug)->first();
        $id = isset($request['id']) ? $request->get('id') : null;
        $page = isset($request['page']) ? $request->get('page') : 0;
        $skip = $this->limit * $page;

        try {
            $data = Products::where([
                ['units', '>', 0],
                ['deleted_at', '=', null],
                ['shop_id', '=', $shop->_id],
                ['_id', '!=', $id],
            ])->skip($skip)->take($this->limit)->orderBy('created_at', 'desc')->get()->keyBy('_id')->toArray();

            return ['data' => $data];
        } catch (\Exception $e) {
            return ['statusCode' => 403, 'error' => $e->getMessage()];
        }
    }
}
