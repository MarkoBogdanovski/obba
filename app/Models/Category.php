<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Laravel\Passport\HasApiTokens;
use Jenssegers\Mongodb\Eloquent\HybridRelations;

class Category extends Eloquent {
    //
    use HasApiTokens, SoftDeletes, HybridRelations;

    protected $connection = 'mongodb';
    protected $table = 'categories';

    public function products()
    {
        return $this->hasMany('App\Models\Products');
    }
}
