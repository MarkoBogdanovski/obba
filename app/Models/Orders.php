<?php
namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Laravel\Passport\HasApiTokens;

class Orders extends Model 
{
    use HasApiTokens, SoftDeletes, HybridRelations;

    protected $connection = 'mongodb';
    protected $primaryKey = "_id";
    /**
     * Validation rules
     * 
     * @var array
     */

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        '_id', 'order', 'shop_id',
        'created_at'
    ];

    protected $hidden = [
        'user_id', 'shop_id', 'updated_at', 'deleted_at'
    ];

    protected $guarded =['created_at','updated_at'];

    protected $table = 'orders';

    public function user()
    {
        return $this->belongsToMany(User::class, 'user_id', 'id');
    }
    public function shop()
    {
        return $this->belongsToMany(Shops::class, 'shop_id', '_id');
    }

    public function product()
    {
        return $this->belongsTo(Products::class, 'product_id', '_id');
    }
}
