<?php
namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Jenssegers\Mongodb\Eloquent\HybridRelations;


Class User extends \TCG\Voyager\Models\User {
    use HasApiTokens, HybridRelations;
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [
        'password', 'remember_token', 'updated_at', 'deleted_at', 'role_id', 'settings', 'email_verified_at'
    ];

    /**
    * The attributes that should be cast to native types.
    *
    * @var array
    */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $table = 'users';

    public function shops()
    {
        return $this->hasMany('App\Models\Shops');
    }

    public function address()
    {
        return $this->hasMany('App\Models\Address');
    }
    
    public function orders()
    {
        return $this->hasMany(Orders::class);
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }
}
