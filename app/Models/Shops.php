<?php
namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Laravel\Passport\HasApiTokens;
use Laravel\Scout\Searchable;
use App\Models\Products;
use App\Models\User;

class Shops extends Model 
{
    use HasApiTokens, SoftDeletes, HybridRelations;

    protected $connection = 'mongodb';
    protected $primaryKey = "_id";

    /**
     * Validation rules
     * 
     * @var array
     */
    public static $rules = [
        'name' => 'required|between:6,72',
    ];

    protected $fillable = [
        '_id', 'phone_number', 'name', 'display_name', 'description',
        'status'
    ];

    protected $hidden = [
        'user_id', 'phone_number', 'updated_at', 'deleted_at', '_method'
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $table = 'shops';

    public function searchableAs()
    {
        return 'shops_index';
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function products()
    {
        return $this->hasMany(Products::class, 'shop_id', '_id');
    }

    public function orders()
    {
        return $this->hasMany(Orders::class, 'shop_id', '_id');
    }
}
