<?php
namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Laravel\Passport\HasApiTokens;
use App\Models\User;

class Address extends Model 
{
    use HasApiTokens, SoftDeletes, HybridRelations;

    protected $connection = 'mongodb';

    /**
     * Validation rules
     * 
     * @var array
     */

    protected $fillable = [
        '_id', 'street', 'number', 'city', 'postal_code', 'status'
    ];

    protected $hidden = [
        'user_id'
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $table = 'address';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', '_id');
    }
}
