<?php
namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Laravel\Passport\HasApiTokens;
use Laravel\Scout\Searchable;
use App\Models\Shops;

/**
 * Class P:roducts
 *
 * @package App
*/
class Products extends Model
{
    use SoftDeletes, HasApiTokens, HybridRelations;

    protected $connection = 'mongodb';
    protected $primaryKey = "_id";
    
    /**
     * Allow mass asignment for listed fields
     * @var array
     */
    protected $fillable = ['_id', 'name', 'description', 'price', 'currency', 'units', 'rating', 'tags', 'shop_id', 'category_id'];

    /**
     * Validation rules
     * @var array
     */
    public static $rules = [
                    '_token' => 'required',
                    'firstname' => 'required|between:3,255',
                    'lastname' => 'required|between:1,255'                    
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [
        'updated_at', 'deleted_at', 
    ];


    /**
     * Custom attributes
     * Note: Custom attribute name can't be the same as the name of model relation
     * @var array
     */
    protected $appends = [];

    protected $table = 'products';

    public function searchableAs()
    {
        return 'dev_Obba';
    }

    public function shop()
    {
        return $this->belongsTo(Shops::class, 'shop_id', '_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
