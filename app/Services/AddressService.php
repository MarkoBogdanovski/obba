<?php

namespace App\Services;

use App\Services\BaseService;
use App\Models\Address;
use MongoDB\BSON\UTCDateTime;
use Auth;

class AddressService extends BaseService
{

    public function __construct(Address $model)
	{
		$this->model = $model;
    }
    
    /**
	 * Get all the model records in the database
	 *
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function all()
	{
	    return $this->model->where([
            ['user_id', '=', Auth::user()->id],
            ['deleted_at', '=', null],
        ])->orderBy('created_at', 'asc')->get();
    }
    
    /**
	  * Create a new model record in the database and return it's id
	 *
	 * @param array $data
	 *
	 * @return \Illuminate\Database\Eloquent\Model
	 */
    public function insertGetId(array $data) 
    {
        $this->unsetClauses();

        return $this->model->insertGetId([
            'user_id' => Auth::user()->id,
            'country' => $data['country'],
            'street' => $data['street'],
            'number' => $data['number'],
            'city' => $data['city'],
            'postal_code' => $data['postalCode'],
            'status' => 0,
            'default' => false,
            'created_at' => new \MongoDB\BSON\UTCDateTime(new \DateTime('now')),
            'deleted_at' => null,
            'updated_at' => null,
        ]);
    }
}