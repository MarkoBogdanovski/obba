<?php

namespace App\Services;

use App\Services\BaseService;
use App\Models\Products;
use App\Models\Category;
use MongoDB\BSON\UTCDateTime;
use Illuminate\Http\Request;
use Auth;

class ProductsService extends BaseService
{

    public function __construct(Products $model)
	{
		$this->model = $model;
    }
    
    /**
	 * Get all the model records in the database
	 *
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function all()
	{
	    //return $this->model->where('user_id', Auth::user()->id)->whereNull('deleted_at')->orderBy('created_at', 'asc')->get();
	    return $this->model->where('user_id', Auth::user()->id)->whereNull('deleted_at')->orderBy('created_at', 'asc')->get();
    }

    
    /**
	 * Get all the model records in the database
	 *
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function paginateItems($skip, $limit)
	{
	    //return $this->model->where('units', '>', 0)->whereNull('deleted_at')->skip($skip)->take($limit)->orderBy('created_at', 'desc')->get()->keyBy('_id');
	    return $this->model->with(['shop'])->where('units', '>', 0)->whereNull('deleted_at')->skip($skip)->take($limit)->orderBy('created_at', 'desc')->get()->keyBy('_id');
    }


     /**
	 * Get all the model records in the database
	 *
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function search(string $q, string $category, int $skip, int $limit) : array
	{
        $categoryId = $this->categoryId($category);

        $total = $this->model->search($q)->where('units', '>', 0)->count();
        
        if($total > 0) {
            if(!isset($category) || $category == 'all') {
                $products = $this->model->search($q)->where('units', '>', 0)->orderBy('created_at', 'desc')->paginate($limit)->keyBy("_id")->toArray();
            } else {
                $category = $categoryId[0];
                $products = $this->model->search($q)->with([
                    'hitsPerPage' => $limit,
                    'filters' => "category_id:{$category}",
                ])->where('units', '>', 0)->orderBy('created_at', 'desc')->paginate($limit)->keyBy("_id")->toArray();
            }        

            $brands = array_count_values(array_column($products, 'brand'));
            $prices = [min(array_column($products, 'price')), max(array_column($products, 'price'))];
            $options = array_values(array_unique(array_column($products, 'options'), SORT_REGULAR));

            return [
                'products' => $products,
                'priceFilter' => $prices,
                'options' => $options,
                'brands' => $brands,
                'totalResults' => $total,
            ];
        } else {
            return [
                "status" => 0,
                "totalResults" => 0
            ];
        }
    }
    
    /**
	  * Create a new model record in the database and return it's id
	 *
	 * @param array $data
	 *
	 * @return \Illuminate\Database\Eloquent\Model
	 */
    public function insertGetId(array $data) 
    {
        $this->unsetClauses();

        $options = [];
        foreach(explode(",", $data['options']) as $option) {
            array_push($options, trim($option));
        }

        $tags = [];
        foreach(explode(",", $data['tags']) as $tag) {
            array_push($tags, trim($tag));
        }

        return $this->model->insertGetId([
            'pid' => base_convert(rand(1000000000, PHP_INT_MAX), 10, 36),
            'name' => $data['name'],
            'category_id' => $data['category'],
            'shop_id' => $data['shop'],
            'brand' => $data['brand'],
            'orders' => (int) 0,
            'optionUnits' => json_decode($data['optionUnits']),
            'units' => (int) $data['units'],
            'price' => (int) $data['price'],
            'options' => $options,
            'tags' => $tags,
            'gallery' => json_decode($data['path']),
            'description' => $data['description'],
            'currency' => "RSD",
            'created_at' => new \MongoDB\BSON\UTCDateTime(new \DateTime('now')),
            'deleted_at' => null,
            'updated_at' => null,
        ]);
    }

    private function categoryId(string $category) : array 
    {
        $categories = Category::all()->keyBy('slug')->toArray();

        $categoryId = [];
        if($category == 'all') {
            foreach($categories as $cat) {
                array_push($categoryId, $cat["_id"]);
            }
        } else {
            array_push($categoryId, $categories[$category]["_id"]);
        }

        return $categoryId;
    }
}