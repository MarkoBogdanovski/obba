<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\User;
use App\Models\Address;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Address::class, function (Faker $faker) {
    return [
    	'country' => $faker->country,
    	'street' => $faker->streetAddress,
    	'postal_code' => $faker->postcode,
    	'city' => $faker->city,
    	'phone_number' => str_replace('+', '', $faker->phoneNumber),
    	'user_id' => User::inRandomOrder()->take(1)->first()->id,
    ];
});
