<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Shops;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Shops::class, function (Faker $faker) 
{
	$name = $faker->unique()->company;
	$slug =  strtolower(str_replace(' ', '-', preg_replace("/[^A-Za-z0-9 ]/", '', $name)));
	$shopDir = "public/content/" . $slug . "/settings/";

	$url = "https://picsum.photos/200";
	$info = pathinfo($url);
	$contents = file_get_contents($url);
	$filename = md5( $info['basename'] . microtime()) . ".jpg";

	Storage::makeDirectory($shopDir);
	Storage::put($shopDir . $filename, $contents);

	$url2 = "https://picsum.photos/1410/400";
	$info2 = pathinfo($url2);
	$contents2 = file_get_contents($url2);
	$filename2 = md5( $info2['basename'] . microtime()) . ".jpg";

	Storage::makeDirectory($shopDir);
	Storage::put($shopDir . $filename2, $contents2);

    return [
    	'name' => $slug,
    	'display_name' =>$name. ' '. $faker->companySuffix,
		'phone_number' => $faker->phoneNumber,
		'status' => 'OPEN',
		'logo' => Storage::url($shopDir . $filename),
		'rating' => [],
		'header' => Storage::url($shopDir . $filename2),
		'description' => $faker->paragraph(10, 30),
		'user_id' => User::inRandomOrder()->take(1)->first()->id,
    ];
});
