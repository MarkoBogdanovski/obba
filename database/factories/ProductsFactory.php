<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Products;
use App\Models\Shops;
use App\Models\Category;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Products::class, function (Faker $faker) {
	$options = [
		['XS', 'S', 'M', 'L'],
		['S', 'M', 'L', 'XL'],
		['XL', 'XXL', 'XXXL'],
		["35", "36", "37", "38", "39"],
		["38", "39", "40", "42", "43"],
		["42", "43", "44", "45.5", "50"],
	];

	$brands = [
		"Nike", "Adidas", "Champion", "SuperDry", "Supreme",
		"Gucci", "Versace"
	];

	$tags = [
		["Majica", "Haos", "Bla bla", "truc truc"],
		["Gilje", "Patike", "Tike"],
		["Naocare", "Care", "Cale"],
	];

	$gallery = [];

	for($i = 0; $i < rand(2, 8); $i++) {
        $gallery[] = ['path' => 'https://obba-products.s3.amazonaws.com/product/'.rand(1,45).'.png'];
	}

	$pid = base_convert(rand(1000000000,PHP_INT_MAX), 10, 36);

    return [
    	'pid' => $pid,
    	'name' => ucfirst($faker->words(rand(3,6), true)),
		'description' => $faker->paragraph(15, 35),
		'price' => $faker->randomNumber(4),
		'currency' => 'RSD',
		'brand' => $faker->randomElement($brands),
		'options' => $faker->randomElement($options),
		'tags' => $faker->randomElement($tags),
		'rating' => [],
		'gallery' => $gallery,
		'units' => $faker->randomNumber(3),
		'orders' => 0,
		'shop_id' => Shops::take(1)->skip(rand(0, 20))->first()->_id,
		'category_id' => Category::take(1)->skip(rand(0, 6))->first()->_id,
		'created_at' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = null)
    ];
});
