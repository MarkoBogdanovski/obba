<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $avatars = "public/users/avatars/initial/";
    $url = "https://thispersondoesnotexist.com/";
	$info = pathinfo($url);
	$contents = file_get_contents($url);
	$filename = md5( $info['basename'] . microtime()) . ".jpg";

	Storage::makeDirectory($avatars);
    Storage::put($avatars . $filename, $contents);
    
    return [
        'name' => $faker->firstName." ".$faker->lastName,
        'avatar' =>  Storage::url($avatars . $filename),
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'owner' => false,
        'password' => bcrypt('password'),
        'remember_token' => Str::random(10),
    ];
});
