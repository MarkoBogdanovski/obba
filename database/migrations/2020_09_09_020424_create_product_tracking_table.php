<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTrackingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_tracking', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('product_id')->index()->unique();
            //$table->unsignedInteger('tracking_data_id')->index()->unique()->nullable();
            //$table->foreign('tracking_data_id')->references('_id')->on('products')->onDelete('cascade');
            $table->bigInteger('views')->nullable()->default(NULL);
            $table->bigInteger('orders')->nullable()->default(NULL);
            $table->bigInteger('purchases')->nullable()->default(NULL);
            $table->bigInteger('ratings')->nullable()->default(NULL);
            $table->bigInteger('reviews')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_tracking');
    }
}
