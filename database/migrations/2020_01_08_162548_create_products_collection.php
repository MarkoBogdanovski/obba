<?php

use Illuminate\Database\Migrations\Migration;
use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsCollection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mongodb')->create('products', function($collection) {
            $collection->index('name');
            $collection->index('description');
            $collection->index('price');
            $collection->index('brand');
            $collection->index('options');
            $collection->index('tags');
            $collection->index('shop_id');
            $collection->index('category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mongodb')->drop('products');
    }
}
