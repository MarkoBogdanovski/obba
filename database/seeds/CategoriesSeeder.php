<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Category;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = Category::create([
            'name' => 'Obuća',
            'slug' => 'obuca',
            'status' => 'default',
            'parent' => NULL,
        ]);
        
        $category = Category::create([
            'name' => 'Odeća',
            'slug' => 'odeca',
            'status' => 'default',
            'parent' => NULL,
        ]);

        $category = Category::create([
            'name' => 'Muška Obuća',
            'slug' => 'muska-obuca',     
            'status' => 'default',
            'parent' => 'obuca',
        ]);

        $category = Category::create([
            'name' => 'Ženska Obuća',
            'slug' => 'zenska-obuca',
            'status' => 'default',
            'parent' => 'obuca',
        ]);

        $category = Category::create([
            'name' => 'Muška Odeća',
            'slug' => 'muska-odeca',   
            'status' => 'default',
            'parent' => 'odeca',
        ]);
        
        $category = Category::create([
            'name' => 'Ženska Odeća',
            'slug' => 'zenska-odeca', 
            'status' => 'default',
            'parent' => 'odeca',
        ]);

        $category = Category::create([
            'name' => 'Aksesoari',
            'slug' => 'aksesoari', 
            'status' => 'default',
            'parent' => NULL,
        ]);
    }
}
