<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['json.response']], function () {
    // public routes
    //Route::get('/test','FirebaseController@index');
    
    Route::post('auth/login', 'AuthController@login');
    Route::post('auth/register', 'AuthController@register');
    Route::post('auth/password', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
    Route::get('auth/password/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('auth/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');
    
    Route::get('categories', 'Api\IndexController@categories');
    Route::get('countries', 'Api\IndexController@countries');

    Route::apiResource('category/{category}/products', 'Api\CategoryProductsController');
    Route::apiResource('products', 'Api\ProductsController')->parameters(['products' => 'id']);
    Route::apiResource('shops', 'Api\ShopsController')->parameters([ 'shops' => 'slug' ]);

    Route::get('shops/{slug}/products', 'Api\ShopProductsController@index');    
    
    //Route::get('search/{category}/{q}', 'Api\ProductsController@search');    
    Route::post('search/{category}/{q}', 'Api\ProductsController@search');    

    Route::post('order', 'Api\OrdersController@store');

    Route::get("user/{username}", "UserController@show");
    //Route::post('order', 'Api\OrdersController@store');

    // private routes
    Route::middleware('auth:api')->group(function () {
        Route::get('auth/user', 'UserController@index');
        Route::patch("user/update", "UserController@update");
        Route::get('myOrders', 'Api\OrdersController@myOrders');
        Route::post('store', 'ProductsController@upload');

        Route::post('product', 'ProductsController@store');
        Route::delete('product/{product}', 'ProductsController@destroy');

        Route::get('shop', 'ShopsController@index');
        Route::post('shop', 'ShopsController@store');
        Route::patch('shop/{shop}', 'ShopsController@update');
        Route::delete('shop/{shop}', 'ShopsController@destroy');
        
        Route::get('orders', 'Api\OrdersController@index');
        //Route::get('orders', 'OrdersController@index');
        Route::get('orders/{sid}', 'Api\OrdersController@show');
        Route::patch('order/{order}', 'OrdersController@update');
        Route::delete('order/{order}', 'OrdersController@destroy');
        
        Route::get('address', 'AddressController@index');
        Route::post('address', 'AddressController@store');
        Route::patch('address/{order}', 'AddressController@update');
        Route::delete('address/{address}', 'AddressController@destroy');
    });
});
