<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test', function () {
    $order = json_decode('{"order":{"order":[{"_id":"5fc6f31b315800000e0079e2","name":"Fuga temporibus itaque vel tenetur sit","price":8292,"currency":"RSD","options":["S","M","L","XL"],"gallery":[{"path":"https://obba-products.s3.amazonaws.com/product/3.png"},{"path":"https://obba-products.s3.amazonaws.com/product/39.png"},{"path":"https://obba-products.s3.amazonaws.com/product/6.png"},{"path":"https://obba-products.s3.amazonaws.com/product/3.png"},{"path":"https://obba-products.s3.amazonaws.com/product/14.png"},{"path":"https://obba-products.s3.amazonaws.com/product/39.png"},{"path":"https://obba-products.s3.amazonaws.com/product/31.png"},{"path":"https://obba-products.s3.amazonaws.com/product/24.png"},{"path":"https://obba-products.s3.amazonaws.com/product/14.png"}],"vendor":{"_id":"5fc6f077191300000f005106","name":"Buvljak"},"option":null,"quantity":1}],"address":{"_id":"5fc6f5d5191300000f005108","country":"Srbija","street":"Nikole Tesle","number":"1","city":"Beograd","postal_code":"11000","status":0,"default":false,"created_at":"2020-12-02T02:03:01.010000Z","deleted_at":null,"updated_at":null},"user_id":374,"comment":"asdasasd","phone":"123123"}}', true);
    event(new App\Events\OrderReceived($order));
    return "Event has been sent!";
});

Auth::routes();

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

//Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

